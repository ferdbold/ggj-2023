// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include <GGJ2023/Characters/RootPart.h>

#include "GenericLevelScriptActor.generated.h"


/**
 * 
 */
UCLASS()
class GGJ2023_API AGenericLevelScriptActor : public ALevelScriptActor
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	TArray<ARootPart*> SpawnedRoots;
};
