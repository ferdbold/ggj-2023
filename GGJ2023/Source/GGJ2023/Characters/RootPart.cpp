// Fill out your copyright notice in the Description page of Project Settings.


#include "RootPart.h"

// Sets default values
ARootPart::ARootPart()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ARootPart::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARootPart::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FVector ARootPart::GetRootGridPosition()
{
	FVector rootLocation = GetOwner()->GetActorLocation();
	return rootLocation / TILE_SIZE;
}

