// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SproutPlayerController.generated.h"

UENUM()
enum PlayerStatus : int
{
	Default     UMETA(DisplayName = "Default"),
	Rooted      UMETA(DisplayName = "Rooted"),
	Dead        UMETA(DisplayName = "Dead"),
	Carrying    UMETA(DisplayName = "Carrying"),
};

UCLASS()
class GGJ2023_API ASproutPlayerController : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASproutPlayerController();



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<PlayerStatus> CurrentState;
};
