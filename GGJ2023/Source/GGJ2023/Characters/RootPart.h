// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RootPart.generated.h"

static const float TILE_SIZE = 100.0f;
static const float TILE_MIDDLE = TILE_SIZE / 2;

UCLASS()
class GGJ2023_API ARootPart : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARootPart();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	FVector GetRootGridPosition();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	bool IsCurvedPart;
};
